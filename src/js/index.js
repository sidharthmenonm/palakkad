import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";
import SmoothScroll from 'smooth-scroll';
import magnificPopup from 'magnific-popup';
import anime from 'animejs/lib/anime.es.js';

window.$ = window.jQuery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;

// $('.video-pop-up').magnificPopup({
//   type: 'iframe',
//   mainClass: 'mfp-fade',
//   removalDelay: 160,
//   preloader: false,
//   fixedContentPos: false
// });

// $.magnificPopup.open({
//   items: {
//     src: 'reshed.webp'
//   },
//   type: 'image'
// });

// $('.img-pop-up').magnificPopup({
//   type: 'image',
//   gallery: {
//       enabled: true
//   }
// });

$(window).on('scroll', function() {
  if ($(window).scrollTop() < 300) {
    $('.site-navigation').removeClass('sticky_header');
  } else {
    $('.site-navigation').addClass('sticky_header');
  }
});

var scroll = new SmoothScroll('a[data-scroll]');

$('.hamburger-menu').on('click', function() {
  $(this).toggleClass('open');
  $('.site-navigation').toggleClass('show');
});

var pathname = location.pathname.split("/");
console.log(pathname);
$('.site-navigation a[href="' + pathname[pathname.length - 1] + '"]').parent().addClass('current-menu-item');

$('#manual2').on('click', function() {
  $('#manual2').magnificPopup({
    items: [
      { src: 'gal-1.jpeg' },
      { src: 'gal-2.jpeg' },
      { src: 'gal-3.jpeg' },
      { src: 'gal-4.jpeg' },
      { src: 'gal-5.jpeg' },
      { src: 'gal-6.jpeg' },
      { src: 'gal-7.jpeg' },
      { src: 'gal-8.jpeg' },
    ],
    gallery: {
      enabled: true
    },
    type: 'image' // this is a default type
  });
})

// var S = $(".S");
// TweenLite.set(".container", { perspective: 500 });
// TweenLite.set(S, { rotationY: -90, transformOrigin: "60% 0% -250px", transformStyle: "preserve-3d" });

// var tl = new TimelineMax({ repeat: -1, yoyo: true, delay: 1, repeatDelay: 1 });

// for (var i = 0; i < S.length; i++) {
//   var NS = S.slice(i);
//   if (i == S.length - 1) {
//     TweenLite.set(NS, { zIndex: -i })
//   }

//   tl.to(NS, 0.05 * i, { rotationY: '+=18', z: 5, backgroundColor: "hsl(75, +=10%, +=4%)", ease: Power0.easeNone })
//     .set(S[i], { boxShadow: ' -7px 10px 40px black', z: 0, backgroundColor: "hsl(75, +=10%, +=4%)" });
// };
// tl.from(S, 0.1, { opacity: 0, color: 'rgba(0,0,0,0)' }, 0).to("#img", 0.5, { opacity: 0.3 }, 0.25);

var S = $(".S");
// for (var i = 0; i < S.length; i++) {
//   $(S[1]).css('transform', 'translateX(' + (i * 80) + ')');
// }

// Curtain --- code

// var count = 0
// $('.S').each(function(item) {
//   $(this).css('transform', `translateX(${5.5*count++}vw)`);
// })

// var count1 = 0;
// $('.R').each(function(item) {
//   $(this).css('transform', `translateX(-${5.5*count1++}vw)`);
// })

// $('.preloader').on('click', function() {

//   anime({
//     targets: '.preloader',
//     translateY: -500,
//     delay: anime.stagger(10), // increase delay by 100ms for each elements.
//     duration: 10000,
//   });

//   anime({
//     targets: '.curtain-container .S',
//     translateX: -10,
//     delay: anime.stagger(10), // increase delay by 100ms for each elements.
//     duration: 20000,
//   });

//   anime({
//     targets: '.curtain-container .R',
//     translateX: 10,
//     delay: anime.stagger(10), // increase delay by 100ms for each elements.
//     duration: 20000,
//     complete: function() {
//       $('.curtain-container').hide();
//     }
//   });

//   setTimeout(() => {
//     $('.curtain-container').hide();
//   }, 3000);
// })